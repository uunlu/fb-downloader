
import puppeteer from "puppeteer";
import express from 'express';
import bodyParser from 'body-parser';
import cheerio from 'cherio';

console.log('index.js starting');

const app = express();
app.use(bodyParser.json());

var cors = require('cors');
app.use(cors());

app.get('/test/', async (req, res, next) => {
  console.log('GET/ test');
  let result = await navigate(false);

  res.json({ response: result })
})

app.get('/video/', async (req, res, next) => {
  console.log('Video fetching has started...');
  let result = await navigate();

  res.json({ response: result })
})

app.listen(2199, () => {
  console.log("fb library downloader is running...");
});

// Helpers

async function navigate(headless = true) {
  const browser = await puppeteer.launch({ headless: headless, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
  try {
    const page = await browser.newPage();
    await page.setViewport({ width: 1024, height: 800 });
    await page.goto(BASE_URL);

    await page.click('._7fc9');
    await page.evaluate(() => {
      Array.from(document.querySelectorAll('div'))
        .find(el => el.textContent === "All").click()
    });

    await page.waitForSelector('video', {
      timeout: 15000
    });

    await page.waitForSelector('video');

    console.log('video selector retrieved');
    await page.waitFor(2000);

    await autoScroll(page);
    await page.waitFor(2000);

    let html = await page.$eval('html', x => x.innerHTML);
    let $ = cheerio.load(html);

    let videos = Array.from($('video')).map(x => $(x).attr('src'));
    console.log(videos);

    if (!videos || !videos.length) return "undefined"
    await page.waitFor(2000);

    // Uncomment if you want to take a screenshot
    // await page.screenshot({ path: 'example.png' });

    console.log(videos);

    return videos;
  } catch (error) {
    console.error(error.message);
    return "error"
  } finally {
    await browser.close();
  }
}

async function autoScroll(page){
  await page.evaluate(async () => {
      await new Promise((resolve, reject) => {
          var totalHeight = 0;
          var distance = 100;
          var timer = setInterval(() => {
              var scrollHeight = document.body.scrollHeight;
              window.scrollBy(0, distance);
              totalHeight += distance;

              if(totalHeight >= scrollHeight){
                  clearInterval(timer);
                  resolve();
              }
          }, 100);
      });
  });
}

// const

const BASE_URL = "https://www.facebook.com/ads/library/?active_status=active&ad_type=all&country=DE&impression_search_field=has_impressions_yesterday&view_all_page_id=106663040899093&sort_data[direction]=desc&sort_data[mode]=relevancy_monthly_grouped"