# Introduction

FB downloader is a simple Node.Js tool that returns json response of download video links from the target URL. 

The target URL is named as BASE_URL in index.js file and it can be altered wit different URLs per user wish. 

## Installation

In order to run the project NodeJs v8 or greater is needed. 
The server can be started: 
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
npm run dev
```

## Usage

```python

To run on localhost, which is the current setting, following end points are available: 
http://localhost:2199/test
http://localhost:2199/video
```

## Endpoints
/video runs on headless browser
#
/test runs on Chrome browser to observe events 

#### Response
Response is JSON with following format:

```bash
{response: [string] }